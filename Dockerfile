FROM rust:1.51

RUN apt-get update \
    && apt-get install --allow-unauthenticated -y -qq \
          cmake \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN rustup component add clippy && rustup component add rustfmt && cargo install cargo-deny --locked && cargo install mdbook --no-default-features --locked
