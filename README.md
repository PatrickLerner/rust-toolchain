# Rust Toolchain

This is a small rust docker image that contains the following components:

- [Rust](https://hub.docker.com/_/rust)
- [rustfmt](https://github.com/rust-lang/rustfmt)
- [Clippy](https://github.com/rust-lang/rust-clippy)
- [cargo-deny](https://github.com/EmbarkStudios/cargo-deny)
- [mdbook](https://github.com/rust-lang/mdBook)

Check it out at: https://hub.docker.com/r/patricklerner/rust-toolchain/tags

## Example usage

`.gitlab-ci.yml`

```
image: patricklerner/rust-toolchain:1.51

stages:
  - test
  - doc

lint_and_test:
  stage: test
  cache:
    paths:
      - cargo/
      - target/
  before_script:
    - export CARGO_HOME="$CI_PROJECT_DIR/cargo"
  script:
    - rustfmt --check $(find src -name "*.rs")
    - cargo deny -L error check
    - cargo clippy --all-targets --all-features -- -D warnings
    - cargo test
  only:
    refs:
      - merge_requests
      - master
    changes:
      - .gitlab-ci.yml
      - Cargo.toml
      - deny.toml
      - "src/**/*.rs"

pages:
  stage: doc
  before_script:
    - export CARGO_HOME="$CI_PROJECT_DIR/cargo"
  script:
    - cargo doc --no-deps
    - mv target/doc public
    - echo '<meta http-equiv="refresh" content="0; url=project_name">' > public/index.html
  artifacts:
    paths:
      - cargo/
      - target/
      - public
  only:
    - master
```
